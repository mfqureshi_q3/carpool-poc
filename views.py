from models import *
from forms import *
from flask import Blueprint, render_template, redirect, url_for, flash, get_flashed_messages, request, session
from urllib.parse import unquote
from json.encoder import JSONEncoder
from json.decoder import JSONDecoder
from base64 import b64encode
from flask_wtf.csrf import CSRFProtect
from flask_wtf.file import FileStorage
from mongoengine.errors import NotUniqueError
from werkzeug.security import check_password_hash, generate_password_hash


bp = Blueprint("view", __name__)

from app import csrf

print("VIEWS.PY")

@bp.route('/', methods=["GET", "POST"])
def index():
    print(Ride.objects())
    if not session.get('uid'):
        return redirect(url_for('view.signin'))
    return render_template('index.html')


@bp.route('/signup', methods=['GET', 'POST'])
def signup():
    form = Signup()
    if form.validate_on_submit():
        user = User(email=form.email.data)
        user.name = form.name.data
        user.password = form.password.data
        user.dp = get_image_as_str(form.dp.raw_data[0])
        try:
            user.save()
        except NotUniqueError:
            flash("Email Already In Use! Please use different email.")
            return redirect(url_for('view.signup'))
        session['uid'] = user.email
        return redirect(url_for('view.index'))
    return render_template('signup.html', form=form)


@bp.route('/signin/', methods=['GET', 'POST'])
def signin():

    form = Signin()
    if form.validate_on_submit():
        user = User.objects(email__exact=form.email.data)
        if user:
            user = user[0]
            if user.password == form.password.data:
                session['uid'] = user.email
                return redirect(url_for('view.index'))
            else:
                return "Incorrect password"
        else:
            flash("Account Not Found! Please Sign Up.")
            return redirect(url_for('view.signup'))
    return render_template('signin.html', form=form)


@bp.route("/signout/")
def signout():
    session['uid'] = None
    return redirect(url_for('view.index'))


@csrf.exempt
@bp.route("/newRide/", methods=["POST"])
def newRide():
    curr_user = User.objects(email__exact=session['uid'])[0]
    print(f"RID={len(Ride.objects())}")
    ride = Ride(RID=len(Ride.objects()))
    ride.driver = curr_user
    ride.start = request.json['start']
    ride.end = request.json['end']
    ride.schedule = request.json['schedule']
    ride.car = request.json['car']
    ride.seats = request.json['seats']
    try:
        ride.save()
        curr_user.update(set__active_rides=ride.RID)
        print("Success")
    except Exception as e:
        print(f"Error add newRide: {e}")
    return "SUCCESS"


@csrf.exempt
@bp.route("/images", methods=["POST"])
def images():
    try:
        print("IMAGE LODAER")
        print(request.form["result"])
        if request.method == "POST":
            print("IMAGE PUT")
        return JSONEncoder().encode({'value':'IMAGELOADER_JSON_RESPONSE'})
    except KeyError:
        return "Failed"


@bp.route("/search/")
@bp.route("/search/<string:text>")
def search(text):
    if not session['uid']: return ''

    if text:
        data = JSONDecoder().decode(unquote(text))
        if session['uid'] == data["email"]:
            return render_template("profile.html", user=User.objects(email__exact=session['uid'])[0], form=Signup())
        user = User.objects(email=data["email"])
        if len(user) == 0:
            return "<div class='text-center'><h1>No User Found!</h1></div>"
        else:
            return render_template("search.html", user=user[0], form=Signup())


@csrf.exempt
@bp.route("/profile/", methods=["GET", "POST"])
def profile():
    if not session.get("uid", None):
        return render_template("profile.html", user=User(), form=Signup())
    user = User.objects(email__exact=session['uid'])[0]
    if request.method == "POST":
        user.update(
            name=request.json['name'],
            password=request.json['password'],
            dp=request.json['dp']
        )
        user.reload()
        session['uid'] = user.email
        print("PROFILE USER RELOAD")
    form = Signup()
    print("PROFILE")
    return render_template("profile.html", user=user, form=form)#f"Username:{user.name}<br>Email:{user.email}<br><img src={user.dp}>"


def get_image_as_str(raw: FileStorage) -> str:
    '''
    Represents raw image data as Data URI that can be used as following in HTML to display image <img src=get_image_as_str(raw)>
    :param raw: raw_data read from WTForms
    :return: ascii string in th following format "data:<content_type>;base64,<image_data>"
    '''
    stream = raw.stream.read()
    print(f"Stream:{stream}")
    if not stream:
        return url_for('static', filename='images/avatar.png')
    return f"data:{raw.content_type};base64,{b64encode(stream).decode('ascii')}"


@csrf.exempt
@bp.route("/address/<cmd>", methods=['POST'])
def address(cmd):
    user = User.objects(email__exact=session['uid'])[0]

    if cmd == 'add':
        addr_type = request.json['type']
        addr = request.json['addrline']+', '+request.json['locality']+', '+request.json['city']+', '+request.json['state']+', India'

        user.address.append(Address(type=addr_type, loc=addr))
        user.save()
        print(f"Address: {addr}")
        return 'true'
    elif cmd == 'delete':
        addr_type = request.form['type']
        user.address.remove(user.address.filter(type=addr_type)[0])
        user.save()
        return 'true'

@bp.route("/myRides")
def myRides():
    user = User.objects(email__exact=session['uid'])[0]
    return render_template("myrides.html", user=user)


@bp.route("/viewRides")
def viewRides():
    return render_template("viewRides.html", rides=Ride.objects())

@csrf.exempt
@bp.route("/joinRide", methods=['GET', 'POST'])
def joinRide():
    if not request.json.get('via'):
        return render_template("join.html", ride=Ride.objects(RID__exact=int(request.json.get('rid')))[0])
    else:
        rid = request.json.get('rid')
        via = request.json.get('via')
        try:
            ride = Ride.objects(RID__exact=int(rid))[0]
            ride.via.append(via)
            ride.save()
        except Exception as e:
            flash(f"Exception {e}")
            return 'false'
        user = User.objects(email__exact=session['uid'])[0]
        user.active_rides.append(ride)
        user.save()
        return 'true'

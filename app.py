from flask import Flask
from flask_wtf.csrf import CSRFProtect
from flask_mongoengine import MongoEngine


SECRET_KEY = "secret key"
app = Flask(__name__)
app.config['SECRET_KEY'] = SECRET_KEY
app.config['MONGODB_SETTINGS'] = {
    'db': 'q3_poc',
    'host': '127.0.0.1',
    'port': 27017
}
db = MongoEngine(app)
csrf = CSRFProtect(app)

from views import bp
app.register_blueprint(bp)

if __name__ == '__main__':

    app.run()

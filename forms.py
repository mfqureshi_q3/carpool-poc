from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed
from wtforms import StringField, PasswordField, validators, IntegerField, FileField, DateTimeField, BooleanField


class Signin(FlaskForm):
    email = StringField('Email', validators=[validators.required(), validators.email()],)
    password = PasswordField('Password', validators=[validators.required(), validators.length(min=8)])


class Signup(Signin):
    name = StringField('Name', validators=[validators.required()], id="name")
    phone = IntegerField('Phone')
    dp = FileField('Choose Profile Picture', validators=[validators.optional(), FileAllowed(('jpg', 'jpeg', 'png', 'gif', 'bmp'), 'Select Valid Image!')], id="dp")


class Account(FlaskForm):
    dp = FileField('Choose Profile Picture', validators=[validators.optional(), FileAllowed(('jpg', 'jpeg', 'png', 'gif', 'bmp'), 'Select Valid Image!')], id="dp")
    addr_type = StringField("Address Type", choices=["Home", "Office", "Others"])
    addr = StringField('Address')


class NewRide(FlaskForm):
    start = StringField("Start")
    end = StringField("End")
    schedule = DateTimeField("Time")
    car = StringField("Car")
    driver = BooleanField("I'll be driving")
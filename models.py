from mongoengine.document import Document
from mongoengine.fields import StringField, EmailField, IntField, ListField, DateTimeField, ReferenceField, EmbeddedDocumentField
from app import db
from datetime import datetime


class Address(db.EmbeddedDocument):
    type = db.StringField(required=True)
    loc = db.StringField(required=True)


class User(db.Document):
    name = db.StringField(required=True)
    email = db.EmailField(required=True, primary_key=True)
    phone = db.IntField()
    password = db.StringField()
    address = db.EmbeddedDocumentListField(Address)
    active_rides = db.ListField(field=ReferenceField('Ride'))
    past_rides = db.ListField(field=ReferenceField('Ride'))
    dp = db.StringField()


class Ride(db.Document):
    RID = db.IntField(required=True, primary_key=True)
    driver = db.ReferenceField(User)
    start = db.StringField()
    end = db.StringField()
    via = db.ListField(field=StringField())
    car = db.StringField()
    schedule = db.DateTimeField(default=datetime.now())
    seats = db.IntField()
    passengers = db.ListField(field=ReferenceField(User))
